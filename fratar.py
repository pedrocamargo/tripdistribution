#-------------------------------------------------------------------------------
# Name:        FRATAR
# Purpose:    Applying a growth factor method
#
# Author:      Pedro Camargo
# Website:    www.xl-optim.com
# Repository:  https://bitbucket.org/pedrocamargo/tripdistribution
#
# Created:     07/01/2014
# Copyright:   (c) Pedro Camargo 2014
# Licence:     GPL
#-------------------------------------------------------------------------------

#The procedures implemented in this code are some of those suggested in
#Modelling Transport, 4th Edition
#Ortuzar and Willumsen, Wiley 2011

# The referred authors have no responsability over this work, of course


import numpy as np
import sys


def main():
    pass
def fratar(matrix, prod, atra, max_error=0.01, max_itera=1000, verbose=True):

    #We guarantee that we enter the iterative process
    error=max_error+1
    itera=0

    #We will ignore all errors as we are treating them explicitly
    np.seterr(all='ignore')


    #We check if dimensions are correct
    if matrix.shape[1]==atra.shape[0] and matrix.shape[0]==prod.shape[0]:

        #And if the vectors are balanced up to 6 decimals
        if round(np.sum(prod),6)==round(np.sum(atra),6):

            if verbose==True: print 'Iterative process has started'

        #Start iterating
            while error>max_error and itera<max_itera:
                if verbose==True: sys.stdout.write('.')

            #computes factors for rows
                marg_rows=tot_rows(matrix)
                row_factor=factor(marg_rows,prod)

            #applies factor
                matrix=np.transpose(np.transpose(matrix)*np.transpose(row_factor))

            #computes factors for columns
                marg_cols=tot_columns(matrix)
                column_factor=factor(marg_cols,atra)


            #applies factor
                matrix=matrix*column_factor

            #increments iterarions and computes errors
                itera=itera+1
                error=max(1-np.min(row_factor),np.max(row_factor)-1, 1-np.min(column_factor),np.max(column_factor)-1)

            if verbose==True: print "\nError of ", round(error*100,5), '% reached after ', itera, ' iterations\n'
            return matrix
        else:
            raise Exception("Production and Attraction vectors are not balanced")
    else:
        raise Exception("Seed matrix, production and attraction vectors do not have compatible dimensions")



def tot_rows(matrix):
    return np.sum(matrix, axis=1)

def tot_columns(matrix):
    return np.sum(matrix, axis=0)

def factor(marginals, targets):
    f=np.divide(targets,marginals)  #We compute the factors
    f[f==np.NINF]=1        #And treat the errors, with the infinites first
    f=f+1                  #and the NaN second
    f=np.nan_to_num(f)     #The sequence of operations is just a resort to
    f[f==0]=2              #use at most numpy functions as possible instead of pure Python
    f=f-1
    return f


if __name__ == '__main__':
    main()
