#-------------------------------------------------------------------------------
# Name:        Gravity calibrating 
# Purpose:    Example on how to use the code for calibrating gravity models
#
# Author:      Pedro Camargo
# Website:    www.xl-optim.com
# Repository:  https://bitbucket.org/pedrocamargo/tripdistribution
#
# Created:     07/01/2014
# Copyright:   (c) Pedro Camargo 2014
# Licence:     GPL
#-------------------------------------------------------------------------------


def main():
    pass

if __name__ == '__main__':
    main()



from Gravity import *
import numpy as np

def logfile(text):
    q=open('Log.txt','a')
    print text
    q.write(text+'\n')
    q.flush()
    q.close()


function="EXPO"


q=open('Log.txt','w') #We clear the logfile
q.close()

print 'Reading entry data'


m=np.loadtxt('input_matrix.csv',delimiter=',', skiprows=1)
c=np.loadtxt('cost.csv',delimiter=',', skiprows=1)

#We find the dimensions of the matrices
#in this case, the cost matrix is complete, so we can deduce the number of zones from it
zones=np.max(c[:,0:2].astype(int))+1

#We create the cost matrix
cost=np.zeros((zones,zones))
cost[c[:,0].astype(int),c[:,1].astype(int)]=c[:,2]


#We create the flow matrix
matrix=np.zeros((zones,zones))
matrix[m[:,0].astype(int),m[:,1].astype(int)]=m[:,2]


logfile('Calibrated function: ' + function)


beta= calibrate_gravity(matrix, cost, function, verbose=True,max_iterations=1000,max_error=0.000001,outmatrix='synthetic.csv', max_cost=10000)
logfile('\n\nResulting Beta: '+str(beta))

