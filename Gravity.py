#-------------------------------------------------------------------------------
# Name:        GRAVITY MODEL 
# Purpose:    Calibrate and apply gravity models
#
# Author:      Pedro Camargo
# Website:    www.xl-optim.com
# Repository:  https://bitbucket.org/pedrocamargo/tripdistribution
#
# Created:     07/01/2014
# Copyright:   (c) Pedro Camargo 2014
# Licence:     GPL
#-------------------------------------------------------------------------------

#The procedures implemented in this code are some of those suggested in
#Modelling Transport, 4th Edition
#Ortuzar and Willumsen, Wiley 2011

# The referred authors have no responsability over this work, of course



import numpy as np
from fratar import fratar
import math
import sys

def main():
    pass


def calibrate_gravity(matrix, cost, function, verbose=True,max_iterations=100,max_error=0.001,max_cost=None, outmatrix=None):
    error=''

    '''The parameters are:
REQUIRED:
    Matrix:         Original flow matrix
    Cost:           Cost matrix
    function:       Which function the user wants to use (EXPO, POWER or GAMMA).  Currently GAMMA is not implemented yet

OPTIONAL:
    verbose:        User can control if he wants information on each iteration during the calibration process
    max_iterations: Maximum iterations in the calibration procedure
    max_error:      Maximum diference between mean costs accepted, as defined in "Modelling Transport"
    max_cost:       Maximum cost bin to be used during calibration.  If any cell has cost larger than max_cost,
                    then such cell in the original matrix will be set to zero and not used in calibration
    outmatrix:      Name for writing the sythetic matrix obtained from applying the model calibrated
    '''

#We compute the matrix marginals
    prod=np.sum(matrix, axis=1)
    atra=np.sum(matrix, axis=0)

    #We need a copy of the cost matrix in case we have a limit for cost
    if max_cost==None:
        use_cost=cost
    else:
        use_cost=(cost<max_cost).astype(int)
        use_cost=use_cost*cost


    #Check is model chosen is valid
    function=function.upper()
    if function!="EXPO" and function!="POWER" and function!="GAMMA": error="Model %s is not valid. Please choose EXPO, POWER or GAMMA" % function

    #Check if dimensions are correct
    if matrix.shape[0]==cost.shape[0] and matrix.shape[1]==cost.shape[1]:"Flow matrix and cost matrix do not have the same dimensions"

    #if there is no error in the data entry
    if len(error)==0:
        gap=max_error+1 #Initialize the error trackers
        itera=0

        #Initialize iterative procedure
        if verbose==True: print 'Initiating Calibration procedure'

        #Steps on page 192 of Modelling Transport
        if function=="EXPO" or function=="POWER":

            #Step 1
            itera=0
            cstar=np.sum(matrix*use_cost)/np.sum(matrix)
            betazero=1/cstar
            synthmatrix=apply_gravity(prod,atra,cost,function, betazero, gamma=None, verbose=False, max_cost=max_cost, max_error=max_error)

            #Step 2
            itera=itera+1
            if verbose==True:  sys.stdout.write('Iteration '+str(itera)+':')

            czero=np.sum(synthmatrix*use_cost)/np.sum(synthmatrix)
            betamminusone=betazero
            betam=betazero*czero/cstar
            gap=abs(1-betam/betamminusone)
            if verbose==True: print str(round(gap*100,5))+'% gap'
            synthmatrix=apply_gravity(prod,atra,cost,function, betam, gamma=None, verbose=False, max_cost=max_cost, max_error=max_error)
            cm=np.sum(synthmatrix*use_cost)/np.sum(synthmatrix)
            cmminusone=czero

        #Starts iterative process from step 3 onwards
        while gap>max_error and itera<max_iterations:
            aux=betam
            betam=((cstar-cmminusone)*betam-(cstar-cm)*betamminusone)/(cm-cmminusone)
            betamminusone=aux

            synthmatrix=apply_gravity(prod,atra,cost,function, betam, gamma=None, verbose=False, max_cost=max_cost, max_error=max_error)
            cm=np.sum(synthmatrix*use_cost)/np.sum(synthmatrix)
            cmminusone=cmminusone

            gap=abs(1-betam/betamminusone)
            itera=itera+1

            if verbose==True:  print 'Iteration '+str(itera)+': '+ str(round(gap*100,5)) + '% gap'

        if function=="GAMMA":
            raise Exception("Model "+function+ " not implemented yet.  Please submit a request if you want this feature implemented or submit your own code!!")
        else:

            if outmatrix!=None:
                w=open(outmatrix,'w')
                print>>w,'O,D,Flow'
                for i in range(matrix.shape[0]):
                    for j in range(matrix.shape[1]):
                        if synthmatrix[i,j]>0: print>>w,str(i)+','+str(j)+','+str(synthmatrix[i,j])
                w.flush()
                w.close()
            return betam
    else:
        raise Exception(error)



def apply_gravity(prod,atra, cost, function, beta, gamma=None, verbose=True, max_cost=None, max_error=0.01):
    error=''

    #Check if vetors are balanced
    if round(np.sum(prod),3) != round(np.sum(atra),3): error="Vetores de producao e atracao nao estao balanceados"

    #Check is model chosen is valid
    function=function.upper()
    if function!="EXPO" and function!="POWER" and function!="GAMMA": error="Model %s is not valid. Please choose EXPO, POWER or GAMMA" % function


    #If there is no implementation error
    if len(error)==0:
        matrix=np.zeros_like(cost)

        #We apply the function
        for i in range(prod.shape[0]):
            matrix[i,:]=apply_function(cost[i,:],prod[i],atra[:],function, beta, gamma)

        #We zero all the cells that are in places were the cost is too high
        #This step is more useful for the calibration, but it can be used for
        #model application as well
        if max_cost !=None:
            a=(cost<max_cost).astype(int)
            matrix=a*matrix

        #We adjust the total of the matrix
        matrix=matrix*np.sum(prod)/np.sum(matrix)

        #And adjust with a fratar
        matrix=fratar(matrix,prod,atra, verbose=False,max_error=max_error)

        return matrix
    else:
        raise Exception(error)

def apply_function(cost,p,a,function,beta,gamma):
    if function=="EXPO":
        return np.exp(-beta*cost)*p*a
    elif function=="POWER":
        f=np.power(cost, -beta)*p*a
        return np.nan_to_num(f)
    elif function=="GAMMA":
        f=np.power(cost, beta)*np.exp(beta*cost)*p*a
        return np.nan_to_num(f)
    else:
        raise Exception("Model "+function+ " is not valid. Please choose EXPO, POWER or GAMMA")

if __name__ == '__main__':
    main()
